FROM node:alpine
LABEL maintainer="Amani Benaoun"

WORKDIR /home/node/app
RUN chown -R node:node /home/node/app
# Copy node package files
COPY package*.json ./

#Installing Packages
RUN npm install

# Copy the source files
COPY . .

# Add any ENV vars if exists
# ENV VAR=Value

# Avoid using root user
# And use node user for security limitations

USER node
#Exposing Server Port
EXPOSE 3000

# Boostrapping the app
CMD ["node", "app.js"]